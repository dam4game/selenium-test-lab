import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.time.Duration;

public class Lab_10_Test_Niepoprawnego_Logowania_Email_DataSource_Test extends SeleniumBaseTest{

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][] {
                {"test@"},
                {"admin"},
                {"@test.pl"},
                {"test@test.com"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest (String wrongEmail) {

        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .typePassword("")
                .submitLoginWithFailure()
                .assertErrorLoginIsShown("The Email field is not a valid e-mail address.");

//        LoginPage loginPage = new LoginPage(driver);
//
//        loginPage.typeEmail(wrongEmail);
//        loginPage.typePassword("");
//        loginPage.submitLoginWithFailure();
//        loginPage.assertErrorLoginIsShown("The Email field is not a valid e-mail address.");

    }
}
