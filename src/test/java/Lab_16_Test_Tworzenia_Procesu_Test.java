import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Lab_16_Test_Tworzenia_Procesu_Test extends SeleniumBaseTest {

@Test
    public void addProcessTest() {
    String processName = UUID.randomUUID().toString().substring(0,10);

    new LoginPage(driver)
            .typeEmail("test@test.com")
            .typePassword("Test1!")
            .submitLogin()
            .goToProcesses()
            .clickAddProcess()
                .typeProcessName(processName)
            .submitCreate()
            .assertProcess(processName,"","");



}

}
