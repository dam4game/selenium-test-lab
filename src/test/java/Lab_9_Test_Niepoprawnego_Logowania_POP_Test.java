import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.time.Duration;

public class Lab_9_Test_Niepoprawnego_Logowania_POP_Test extends SeleniumBaseTest{

    @Test
    public void correctLoginTest() {

        new LoginPage(driver)
                .typeEmail("test2@test.com")
                .typePassword("Test1!")
                .submitLoginWithFailure()
                .assertErrorLoginIsShown("Invalid login attempt.");

//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.typeEmail("test2@test.com");
//        loginPage.typePassword("Test1!");
//        loginPage.submitLoginWithFailure();
//        loginPage.assertErrorLoginIsShown("Invalid login attempt.");
//

    }
}