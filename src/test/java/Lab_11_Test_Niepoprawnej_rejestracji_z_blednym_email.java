import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;

import java.util.List;

public class Lab_11_Test_Niepoprawnej_rejestracji_z_blednym_email extends SeleniumBaseTest{

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][] {
                {"test@"},
                {"admin"},
                {"@test.pl"},
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest (String wrongEmail) {

        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .typePassword("")
                .submitLoginWithFailure()
                .assertErrorLoginIsShown("The Email field is not a valid e-mail address.");


//        RegisterPage registerPage = new RegisterPage(driver);
//
//        registerPage.typeEmail(wrongEmail);
//        registerPage.typePassword("");
//        registerPage.submitLoginWithFailure();
//
//        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
//        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");
//
//        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
//        boolean doesErrorExists = validationErrors
//                .stream()
//                .anyMatch(validationError -> validationError.getText().equals("The Email field is not a valid e-mail address."));
//        Assert.assertTrue(doesErrorExists);

        driver.quit();
    }

}
