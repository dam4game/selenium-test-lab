import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_13_Test_HomePage_Menu_Test extends SeleniumBaseTest{

    @Test
    public void gotToProcessesTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses();



    }

    @Test
    public void gotToCharasteristicsTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToCharacteristics();

    }

    @Test
    public void gotToDashboardsTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToDashboard();

    }
}
