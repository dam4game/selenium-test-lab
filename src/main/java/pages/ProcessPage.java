package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import java.util.List;

public class ProcessPage extends HomePage {


    public ProcessPage(final WebDriver driver) {
        super(driver);
    }

    private String GENERIC_PROCESS_ROW_XPATH = "//td[text()='%s']/..";

    @FindBy(xpath = "//h3[text()='Processes']")
    private WebElement processElm;

    @FindBy(xpath = "//a[text()='Add new process']") //linkText = "Add new process"
    private WebElement createNewProcElm;

    @FindBy(css = "//tbody/tr[last()]/td[1]/text()")
    private WebElement processCreatedElm;

    @FindBy(css = ".flash-message")
    private WebElement procCreatedMsg;

    public ProcessPage assertProcessesUrl(String Url) {

        Assert.assertEquals(driver.getCurrentUrl(), Url);

        return this;
    }

    public ProcessPage assertProcessHeader() {
        Assert.assertTrue(processElm.getText().contains("Processes"), "Process element text: '" +
                processElm.getText() + "' does not contain word 'Processes'");

        return this;
    }

    public ProcessPage assertProcess(String procName, String procDesc, String procNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, procName);

        WebElement processRow = driver.findElement(By.xpath(processXpath));

        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();

        Assert.assertEquals(actDescription, procDesc);
        Assert.assertEquals(actNotes, procNotes);

        Assert.assertTrue(procCreatedMsg.getText().contains("Process has been created"));
//    Assert.assertTrue(processCreatedElm.getText().contains(procName));
        return this;
    }


    public CreateProcessPage clickAddProcess() {
        createNewProcElm.click();
        return new CreateProcessPage(driver);
    }


    public ProcessPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }
}