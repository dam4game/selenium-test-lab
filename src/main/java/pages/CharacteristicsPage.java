package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage{

    public CharacteristicsPage(final WebDriver driver) {
        super(driver);
    }
    @FindBy(css = ".title_left>h3")
    WebElement characteristicElm;

    public CharacteristicsPage assertCharasteristicUrl(String Url) {
        Assert.assertEquals(driver.getCurrentUrl(),"http://localhost:4444/Characteristics");
        return this;
    }

    public CharacteristicsPage assertCharacteristicHeader() {

        Assert.assertEquals(characteristicElm.getText(), "Characteristics");
        return this;
    }

}
