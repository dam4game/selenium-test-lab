package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateCharacteristicPage extends CharacteristicsPage{

    public CreateCharacteristicPage(final WebDriver driver) {
        super(driver);
    }

    @FindBy(id="ProjectId")
    private WebElement ProjectSlc;

    @FindBy(id="Name")
    private WebElement nameTxt;

    @FindBy(id="LowerSpecificationLimit")
    private WebElement lowerSpecLimitTxt;

    @FindBy(id="UpperSpecificationLimit")
    private WebElement upperSpecLimitTxt;

    @FindBy(id="HistogramBinCount")
    private WebElement histBinCountTxt;

    @FindBy(css = ".btn-success")
    private WebElement createBtn;


    @FindBy(xpath = "//a[text()='Add new characteristic']") //linkText = "Add new process"
    private WebElement createNewProcElm;



    public CreateCharacteristicPage clickAddProcess() {
        createNewProcElm.click();
        return new CreateCharacteristicPage(driver);
    }

}
