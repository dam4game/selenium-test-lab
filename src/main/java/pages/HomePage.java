package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;

public class HomePage {
    protected WebDriver driver;



    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(css = ".menu-workspace")
    WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects]")
    WebElement processMenu;

    @FindBy(css = "a[href$=Characteristics]")
    WebElement characteristicMenu;

    @FindBy(xpath = "//a[text()='Dashboard']")
    WebElement dashboardMenu;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HomePage assertWelcomeElementIsShown() {

        Assert.assertTrue(welcomeElm.isDisplayed(), "Welcome element is not shown.");
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"), "Welcome element text: '" +
                welcomeElm.getText() + "' does not contain word 'Welcome'");
        return this;
    }

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public ProcessPage goToProcesses() {

        if (!isParentExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(processMenu));

        processMenu.click();

        return new ProcessPage(driver);
    }

    public CharacteristicsPage goToCharacteristics() {
        if (!isParentExpanded(workspaceNav)) {
            workspaceNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5)); // dla umożliwienia rozwinięcia sie menu
        wait.until(ExpectedConditions.elementToBeClickable(characteristicMenu));

        characteristicMenu.click();

        return new CharacteristicsPage(driver);
    }

    public DashboardsPage goToDashboard() {

        if (!isParentExpanded(homeNav)) {
            homeNav.click();
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));

        dashboardMenu.click();

        return new DashboardsPage(driver);
    }

}
