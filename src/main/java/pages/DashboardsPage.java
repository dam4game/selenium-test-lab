package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardsPage extends HomePage {

    public DashboardsPage(final WebDriver driver) {
     super(driver);
    }

//    @FindBy(css = "//tbody //p[1]/text()")
//    WebElement demoElm;

    @FindBy(xpath = "//h2[text()='DEMO PROJECT']")
    private WebElement demoProjectHeader;

    @FindBy(linkText = "Create your first process")
    private WebElement createFirstProjectBtn;


    public DashboardsPage assertDashboardUrl(String Url) {

        Assert.assertEquals(driver.getCurrentUrl(), "http://localhost:4444/");
        return this;
    }

    public DashboardsPage assertDemoProjectIsShown() {
//        System.out.println(demoElm.getText());
        Assert.assertEquals(demoProjectHeader.getText(), "DEMO PROJECT");
        return this;
    };

}
