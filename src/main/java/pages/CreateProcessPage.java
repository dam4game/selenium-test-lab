package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessPage extends ProcessPage {

    CreateProcessPage(final WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".btn-success")
    private WebElement createBtn;

    @FindBy(css = "#Name")
    private WebElement processNameElm;

    @FindBy(css = "#Notes")
    private WebElement processNotesElm;

    @FindBy(css = ".field-validation-error[data-valmsg-for=Name]")
    private WebElement nameError;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBtn;

    public CreateProcessPage typeProcessName(String ProcessName) {
        processNameElm.clear();
        processNameElm.sendKeys(ProcessName);
        return this;
    }

    public CreateProcessPage typeProcessDesc(String ProcessDesc) {
        processNameElm.clear();
        processNameElm.sendKeys(ProcessDesc);
        return this;
    }

    public CreateProcessPage typeProcessNotes(String ProcessNotes) {
        processNameElm.clear();
        processNameElm.sendKeys(ProcessNotes);
        return this;
    }

    public ProcessPage submitCreate() {
        createBtn.click();
        return this;
    }

    public CreateProcessPage submitCreateWithFailure() {
        createBtn.click();
        return this;
    }

    public CreateProcessPage assertProcessNameWithError(String expErrorMsg) {
        Assert.assertEquals(nameError.getText(), expErrorMsg);
        return this;
    }

    public ProcessPage backToList() {
        backToListBtn.click();
        return new ProcessPage(driver);
    }

}
